<nav class="container">
    <div class="logo">
        <a href="/">
            <img src="./assets/images/logo/lion-of-informatics.webp" alt="Logo Lion of Informatics" loading="lazy">
        </a>
    </div>
    <ul class="nav-links poppins">
        <li>
            <a href="">About</a>
        </li>
        <li>
            <a href="">Program</a>
        </li>
        <li>
            <a href="https://careers.lionofinformatics.net/">Career</a>
        </li>
        <li>
            <a href="https://lionofinformatic.blogspot.com/">Blog</a>
        </li>
        <li>
            <a href="" class="button">Sign In</a>
        </li>
    </ul>
    <div class="hamburger" onclick="toggleHamburger(this)">
        <span></span>
        <span></span>
        <span></span>
    </div>
</nav>