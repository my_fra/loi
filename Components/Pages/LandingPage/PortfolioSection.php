<section id="portfolios-section">
        <div class="portfolio">
            <img loading="lazy" class="client-result" src="./assets/images/portfolio/rizqullah.webp" alt="rizqullah.co">
            <div class="body">
                <span class="client-logo">
                    <img loading="lazy" src="./assets/images/client-logo/logo-rizqullah.webp" alt="viding.co">
                </span>
                <span class="portfolio-desc">
                    <h3 class="poppins">Rizqullah</h3>
                    <p class="poppins">The Best Busssines Deelopment Partner</p>
                </span>
                <span class="badges">
                    <span class="badge review">
                        <img loading="lazy" src="./assets/icons/star.svg" alt="start-icon">
                        <span class="poppins">4.5</span>
                    </span>
                    <span class="badge love">
                        <img loading="lazy" src="./assets/icons/love.svg" alt="love-icon">
                        <span class="poppins">100</span>
                    </span>
                </span>
            </div>
        </div>
        <div class="portfolio">
            <img loading="lazy" class="client-result" src="./assets/images/portfolio/viding.webp" alt="viding.co">
            <div class="body">
                <span class="client-logo">
                    <img loading="lazy" src="./assets/images/client-logo/logo-viding.webp" alt="viding.co">
                </span>
                <span class="portfolio-desc">
                    <h3 class="poppins">Viding</h3>
                    <p class="poppins">Platform Virtual Wedding</p>
                </span>
                <span class="badges">
                    <span class="badge review">
                        <img loading="lazy" src="./assets/icons/star.svg" alt="start-icon">
                        <span class="poppins">4.5</span>
                    </span>
                    <span class="badge love">
                        <img loading="lazy" src="./assets/icons/love.svg" alt="love-icon">
                        <span class="poppins">100</span>
                    </span>
                </span>
            </div>
        </div>
        <div class="portfolio">
            <img loading="lazy" class="client-result" src="./assets/images/portfolio/naura.webp" alt="naura.id">
            <div class="body">
                <span class="client-logo">
                    <img loading="lazy" src="./assets/images/client-logo/logo-naura.webp" alt="naura.id">
                </span>
                <span class="portfolio-desc">
                    <h3 class="poppins">Naura</h3>
                    <p class="poppins">Platform Marketplace Umroh</p>
                </span>
                <span class="badges">
                    <span class="badge review">
                        <img loading="lazy" src="./assets/icons/star.svg" alt="start-icon">
                        <span class="poppins">4.5</span>
                    </span>
                    <span class="badge love">
                        <img loading="lazy" src="./assets/icons/love.svg" alt="love-icon">
                        <span class="poppins">100</span>
                    </span>
                </span>
            </div>
        </div>
        <div class="portfolio">
            <img loading="lazy" class="client-result" src="./assets/images/portfolio/gluxent.webp" alt="gluxent.co">
            <div class="body">
                <span class="client-logo">
                    <img loading="lazy" src="./assets/images/client-logo/logo-gluxent.webp" alt="gluxent.co">
                </span>
                <span class="portfolio-desc">
                    <h3 class="poppins">Gluxent</h3>
                    <p class="poppins">Live With Style or Die In Vain</p>
                </span>
                <span class="badges">
                    <span class="badge review">
                        <img loading="lazy" src="./assets/icons/star.svg" alt="start-icon">
                        <span class="poppins">4.5</span>
                    </span>
                    <span class="badge love">
                        <img loading="lazy" src="./assets/icons/love.svg" alt="love-icon">
                        <span class="poppins">100</span>
                    </span>
                </span>
            </div>
        </div>
</section>