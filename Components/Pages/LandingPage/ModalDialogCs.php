<div class="modal-dialog-cs d-none" id="modal-dialog-cs">
    <div class="modal-head">
        <div class="text-head-wrapper">
            <div class="text-head-wrap poppins">
                <h3>Anda Butuh Bantuan?</h3>
                <h3>Kirim Pesan Kepada Kami</h3>
            </div>
            <div class="btn-head-wrap">
                <button onclick="togglePlayAudio(this)" id="button-toggle-music" everClicked="false">
                    <i class="zmdi zmdi-play"></i>
                </button>
            </div>
        </div>
        <p class="desc-text-head poppins">Klik Salah Satu Dari Specialist Kami Dibawah Ini.</p>
    </div>
    <div class="modal-body">
        <a href="https://wa.me/6282140572544" target="_blank" class="card-modal-body">
            <div class="icon-area">
                <img src="/assets/icons/user-cs.svg" alt="user-cs">
                <span class="bullet"></span>
            </div>
            <div class="text-area poppins">
                <p>Silahkan Bertanya, Kami Siap Membantu Anda Untuk Memberikan Pelayanan Terbaik</p>
            </div>
        </a>
        <a href="https://wa.me/6282140572544" target="_blank" class="card-modal-body">
            <div class="icon-area">
                <img src="/assets/icons/user-cs.svg" alt="user-cs">
                <span class="bullet"></span>
            </div>
            <div class="text-area poppins">
                <p>Berminat Menjadi Mitra Kami? Atau Hanya Sekedar Untuk Investasi?</p>
            </div>
        </a>
    </div>
</div>