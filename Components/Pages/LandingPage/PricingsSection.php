<?php

$prices = [
    //Essential
    [
        'title'         => 'Essential',
        'description'   => 'We Provides for Collage Student Indonesia',
        'strike-price'  => 'IDR 5 Jt',
        'real-price'    => 'IDR 3 Jt',
        'timeline'      => 'Estimated Price',
        'data' => [
            [
                'title-feature' => 'Web / App',
                'items'         => [
                    [
                        'title-sub-feature' => 'User Office',
                        'desc-sub-feature'  => '(Landing Page)',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Back Office',
                        'desc-sub-feature'  => '(CMS)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
            [
                'title-feature' => 'Domain & Server',
                'items'         => [
                    [
                        'title-sub-feature' => 'Shared Host',
                        'desc-sub-feature'  => '500GB SSD Storage <br> Unlimited Database, Bandwith',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Domain TLD',
                        'desc-sub-feature'  => '(.me/.dev/.tech/.xyz)',
                        'logo-sub-feature'  => NULL,
                        // 'logo-sub-feature'  => './../../../assets/images/vendors/namecheap.svg',
                    ],
                ],
            ],
            [
                'title-feature' => 'Document',
                'items'         => [
                    [
                        'title-sub-feature' => 'Undergraduate Thesis/Minithesis',
                        'desc-sub-feature'  => NULL,
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
        ],
    ],
    //Advanced
    [
        'title'         => 'Advanced',
        'description'   => 'We Provides for SMEs / Start Up Indonesia',
        'strike-price'  => 'IDR 35 Jt',
        'real-price'    => 'IDR 10 Jt',
        'timeline'      => 'Estimated Price',
        'data' => [
            [
                'title-feature' => 'Web / App',
                'items'         => [
                    [
                        'title-sub-feature' => 'User Office',
                        'desc-sub-feature'  => '(Company Profile/Landing Page)',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Back Office',
                        'desc-sub-feature'  => '(CMS/LMS)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
            [
                'title-feature' => 'Domain & Server',
                'items'         => [
                    [
                        'title-sub-feature' => 'VPS Host',
                        'desc-sub-feature'  => 'Unlimited SSD Storage <br> Unlimited Database, Bandwith, Email',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Domain TLD',
                        'desc-sub-feature'  => '(.com/.net/.org)',
                        'logo-sub-feature'  => NULL,
                        // 'logo-sub-feature'  => './../../../assets/images/vendors/namecheap.svg',
                    ],
                ],
            ],
            [
                'title-feature' => 'Design & Video',
                'items'         => [
                    [
                        'title-sub-feature' => 'Design Logo',
                        'desc-sub-feature'  => NULL,
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Design Feed Social Media',
                        'desc-sub-feature'  => '(Instagram,Tiktok,Facebook,Youtube)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
        ],
    ],
    //Profesional
    [
        'title'         => 'Ultimate',
        'description'   => 'We Provides for Corporate & Goverment Indonesia',
        'strike-price'  => 'IDR 75 Jt',
        'real-price'    => 'IDR 25 Jt',
        'timeline'      => 'Estimated Price',
        'data' => [
            [
                'title-feature' => 'Web / App',
                'items'         => [
                    [
                        'title-sub-feature' => 'User Office',
                        'desc-sub-feature'  => '(Company Profile/Landing Page)',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Back Office',
                        'desc-sub-feature'  => '(CMS/ERP/CRM/LMS)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
            [
                'title-feature' => 'Domain & Server',
                'items'         => [
                    [
                        'title-sub-feature' => 'Cloud Host',
                        'desc-sub-feature'  => 'Unlimited Database <br> Unlimited Bandwith, Storage, Email',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Domain TLD',
                        'desc-sub-feature'  => '(.co.id/.gov.id/org.id/ac.id)',
                        'logo-sub-feature'  => NULL,
                        // 'logo-sub-feature'  => './../../../assets/images/vendors/namecheap.svg',
                    ],
                ],
            ],
            [
                'title-feature' => 'Design & Video',
                'items'         => [
                    [
                        'title-sub-feature' => 'Design Logo',
                        'desc-sub-feature'  => NULL,
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Design Feed Social Media',
                        'desc-sub-feature'  => '(Instagram,Tiktok,Facebook,Youtube)',
                        'logo-sub-feature'  => NULL,
                    ],
                    [
                        'title-sub-feature' => 'Video Explainer',
                        'desc-sub-feature'  => '(Company Profile/Review Product)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
            [
                'title-feature' => 'Digital Marketing',
                'items'         => [
                    [
                        'title-sub-feature' => 'Digital Advertise',
                        'desc-sub-feature'  => '(Facebook/Instagram/Google/Youtube)',
                        'logo-sub-feature'  => NULL,
                    ],
                ],
            ],
        ],
    ],
];

?>

<section id="pricings" class="container">
    <?php foreach( $prices as $i=>$price ) { ?>
        <div class="pricing <?= $i == 1 ? 'active' : '' ?>">
            <div class="head raleway">
                <h2><?= $price['title'] ?></h2>
                <p><?= $price['description'] ?></p>
            </div>
            <div class="price-area poppins">
                <h4 class="strike-price"><?= $price['strike-price'] ?></h4>
                <div class="real-price">
                    <h3><?= $price['real-price'] ?></h3>
                    <span><?= $price['timeline'] ?></span>
                </div>
            </div>
            <?php foreach( $price['data'] as $key => $product ) { ?>
                <ul class="features poppins">
                    <li class="feature " onclick="toggleShowSubFeatures(this)" data-target="<?= $key ?>">
                        <span class="title-feature"><?= $product['title-feature'] ?></span>
                        <span><i class="zmdi zmdi-chevron-up"></i></span>
                    </li>
                    <?php foreach( $product['items'] as $item ) { ?>
                    <ul class="sub_features d-none" id="<?= $key ?>">
                        <li class="sub_feature">
                            <span class="title-sub-feature"><?= $item['title-sub-feature'] ?><span><?= $item['desc-sub-feature'] ?></span></span>
                            <?php if($item['logo-sub-feature'] != NULL)  { ?>
                                <div class="vendor">
                                    <img loading="lazy" src="<?= $item['logo-sub-feature'] ?>" alt="namecheap">
                                    <span class="info"><i class="zmdi zmdi-info-outline"></i></span>
                                </div>
                            <?php } else { ?>
                                <span class="info"><i class="zmdi zmdi-info-outline" style="color: #444;"></i></span>
                            <?php } ?>
                        </li>
                    </ul>
                    <?php } ?>
                </ul>
            <?php } ?>
            <button href="https://wa.me/6282140572544" class="purchase-now poppins">Purchase Now</button>
        </div>
    <?php } ?>
</section>
<br>
<br>
