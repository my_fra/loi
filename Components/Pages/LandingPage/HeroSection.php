<section id="hero" class="hero-slider">
    <div class="hero-cover hero-fade-background" style="background-image: url('/assets/images/background/hero/Lioners-background-1.webp');">
        <div class="inner-hero-cover container">
        </div>
    </div>
    <div class="hero-cover hero-fade-background" style="background-image: url('/assets/images/background/hero/Lioners-background-3.webp');">
        <div class="inner-hero-cover container">
        </div>
    </div>
    <div class="hero-cover hero-fade-background" style="background-image: url('/assets/images/background/hero/Lioners-background-2.webp');">
        <div class="inner-hero-cover container">
            <div class="teams-wrapper">
                <div class="right-side-teams">
                    <div class="teams" id="particles-js">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="hero" class="hero-content">
    <div class="hero-cover">
        <div class="inner-hero-cover container bg-transparent">
            <div class="wrapper-content">
                <div class="text-heading-wrapper poppins" style="position: relative;">
                    <div class="atas-bullet">
                        <div class="inner-atas-bullet">
                            <div class="bullets">
                                <span class="bullet-pointer active"></span>
                                <span class="bullet-pointer"></span>
                                <span class="bullet-pointer"></span>
                            </div>
                        </div>
                        <div class="text-hero">
                            <h2 class="fade-in">We deliver digital innovation and bright solutions for your business</h2>
                            <h2 class="fade-in d-none">We play around with communities. Deliver bright solutions to everyone</h2>
                            <h2 class="fade-in d-none">We collaborate and develop innovation with leading industries to deliver stunning products and services</h2>
                        </div>
                    </div>
                    <div class="button-wrapper">
                        <a href="">Get Started</a>
                        <button><img loading="lazy" src="./assets/icons/play.svg" alt=""><span>Watch Video</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>