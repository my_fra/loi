<section id="about-purpose-media-section">
    <div class="card-about poppins">
        <div class="inner-card-about" style="background-image: url('./assets/images/background/about/life-of-lioners.webp');">
            <div class="background-mult"></div>
        </div>
        <div class="inner-text-card-about">
            <h3>About Us</h3>
            <p>Lion Of Informatics is a startup that aims to develop a technology ecosystem in Indonesia. Provides various products & services for the needs of companies / startups. create a informatics community and education to realize Indonesia as a digital country in 2035.</p>
            <a href="">Learn more <img loading="lazy" src="./assets/icons/arrow-right.svg" alt="arrow-right"></a>
        </div>
    </div>
    <div class="purposes-wrapper">
        <div class="purpose">
            <div>
                <img loading="lazy" src="./assets/images/illustration/about/purpose-product.svg" alt="">
            </div>
            <div class="text-purpose-wrapper">
                <h4 class="poppins">Product</h4>
                <p class="raleway">We collaborate and develop innovation with leading industries to deliver stunning products.</p>
            </div>
        </div>
        <div class="purpose">
            <div>
                <img loading="lazy" src="./assets/images/illustration/about/purpose-service.svg" alt="purpose-service">
            </div>
            <div class="text-purpose-wrapper">
                <h4 class="poppins">Service</h4>
                <p class="raleway">We deliver digital innovation and bright solutions for your business</p>
            </div>
        </div>
        <div class="purpose">
            <div>
                <img loading="lazy" src="./assets/images/illustration/about/purpose-community.svg" alt="">
            </div>
            <div class="text-purpose-wrapper">
                <h4 class="poppins">Community</h4>
                <p class="raleway">Not Just Create Things.
                    We Build The Future. We play around with communities
                    . deliver bright solutions to everyone</p>
            </div>
        </div>
        <div class="purpose">
            <div>
                <img loading="lazy" src="./assets/images/illustration/about/purpose-academy.svg" alt="">
            </div>
            <div class="text-purpose-wrapper">
                <h4 class="poppins">Academy</h4>
                <p class="raleway">We break ourself apart just to make sure, we can do the best to create the bright future</p>
            </div>
        </div>
    </div>
    <div class="media-coverage">
        <h3 class="raleway">Media Coverage</h3>
        <hr>
        <div class="media-coverages-wrapper">
            <a href="https://www.merdeka.com/" target="_blank">
                <img loading="lazy" src="./assets/images/media-pers/merdeka.webp" alt="merdeka">
            </a>
            <a href="https://youngster.id/" target="_blank">
                <img loading="lazy" src="./assets/images/media-pers/youngster.webp" alt="youngster">
            </a>
            <a href="https://www.detik.com/" target="_blank">
                <img loading="lazy" src="./assets/images/media-pers/detikcom.svg" alt="detikCom">
            </a>
            <a href="https://www.indotelko.com/" target="_blank">
                <img loading="lazy" src="./assets/images/media-pers/indotelko.webp" alt="indotelko">
            </a>
            <a href="https://radar-berita.com/" target="_blank">
                <img loading="lazy" src="./assets/images/media-pers/radar.webp" alt="radar">
            </a>
        </div>
    </div>
</section>