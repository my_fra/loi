<?php

$reviews = [
    [
        'logo' => './assets/images/client-logo/logo-rizqullah.webp',
        'vendor' => 'rizqullah.co',
        'description' => 'One Stop Bussines Solutions',
        'testimonial' => 'Not only do they focus on how the website is finished, but they also make a valuable addition to the growth of your business with the platform.'
    ],
    [
        'logo' => './assets/images/client-logo/logo-viding.webp',
        'vendor' => 'viding.co',
        'description' => 'One Stop Virtual Wedding',
        'testimonial' => 'More than what we need, it looks very experienced and professional. we grow with friendly and interactive support 24/7.'
    ],
    [
        'logo' => './assets/images/client-logo/logo-naura.webp',
        'vendor' => 'naura.id',
        'description' => 'One Stop Umroh Solution',
        'testimonial' => 'Support is comfortable and reliable. The principles of clean code and best practice, make the development of platform features very possible for the long term. we recomend it'
    ],
    [
        'logo' => './assets/images/client-logo/logo-gluxent.webp',
        'vendor' => 'gluxent.co',
        'description' => 'One Stop Mens Grooming',
        'testimonial' => 'It is done by an experienced and professional team of developers. Provide comfort in communicating for every problem.'
    ],

]

?>

<section id="review-section">
    <h2 class="raleway">Review</h2>
    <div class="reviews">
        <?php foreach( $reviews as $review ) { ?>
            <div class="review poppins">
                <div class="client">
                    <span class="logo">
                        <img loading="lazy" src="<?= $review['logo'] ?>" alt="<?= $review['vendor'] ?>">
                    </span>
                    <div class="client-desc">
                        <h4><?= $review['vendor'] ?></h4>
                        <span class="description"><?= $review['description'] ?></span>
                        <span class="rate-review">
                            <img loading="lazy" src="./assets/icons/star.svg" alt="start-icon">
                            <span>4.5</span>
                        </span>
                    </div>
                </div>
                <p><?= $review['testimonial'] ?></p>
            </div>
        <?php } ?>
    </div>
</section>