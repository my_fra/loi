<?php

$services = [
    [
        'title' => 'Graphic / 
        Illustration Design',
        'description' => 'Unique and distinctive design has a very important role to increase the brand image in the eyes of costumers.',
        'hex-color' => '#23467C'
    ],
    [
        'title' => 'Photography /
        Videography',
        'description'   => 'Serving Product Photos, Photoshots, Weddings, Clothing Models, Company Profiles, Documentation and more.',
        'hex-color' => '#393E46'
    ],
    [
        'title' => 'UI-UX / 
        3D Modeling',
        'description'   => 'Design a professional interface to make it easier to use Web/App in accordance with your expected goals.',
        'hex-color' => '#C32BAD'
    ],
    [
        'title' => 'Editing /
        Animation Video',
        'description'   => 'Video editing services become a complete and interesting work for the audience to get amazing results.',
        'hex-color' => '#615F8B'
    ],
    [
        'title' => 'Web / App
        Development',
        'description'   => 'Website development homepage / admin panel with custom CMS, ERP, LMS to provide the best solution at the best price. <br> <br> <br> <br>',
        'hex-color' => '#4267B2'
        // 'hex-color' => '#041562'
    ],
    [
        'title' => 'AR / VR 
        Development',
        'description'   => 'Build AR App that extend real-world scenes with amazing features providing an interactive user experience. <br> <br <br> <br> <br>',
        'hex-color' => '#139487'
    ],
    [
        'title' => 'Domain & <br> Server',
        'description'   => 'We offer cheap & quality domains with a wide selection of Local and International domains supported by Trusted Registrant partners. <br> <br> <br>',
        'hex-color' => '#F25022'
    ],
    [
        'title' => 'Bug Hunter /
        Security Testing',
        'description'   => 'We have experience in finding security gaps contained in some digital systems with CEH & CSCU certification from EC-Council so that its capabilities are unquestionable..',
        'hex-color' => '#222831'
    ],
    [
        'title' => 'Service / Build 
        Hardware',
        'description'   => 'Provides services hardware supply/assembly Quality and Guaranteed . PC, RIG Miner Cryptocurrency, Modem Pool, Servers.',
        'hex-color' => '#395B64'
    ],
    [
        'title' => 'Network
        Development',
        'description'   => 'We Provides Network Design, Implementation, Configuration, Optimization ISP and Building Data Center with Cisco / Mikrotik.',
        'hex-color' => '#1A1A40'
    ],
    [
        'title' => 'Digital <br> Marketing',
        'description'   => 'Digital marketing strategies are supported by professionals with databases and business intelligence. Make your brand have what it dreams',
        'hex-color' => '#FFB900'
    ],
    [
        'title' => 'Premium Digital
        Account',
        'description'   => 'Enjoy premium services from the platform such as Netflix, Youtube, Disney+, Spotify, etc. Affordable and Guaranteed from official or unofficial providers.',
        'hex-color' => '#E60023'
    ],
];

?>

<section id="service-and-product" class="container">
    <div class="tab-headers">
        <h2 class="text-headers volkhov">
            <span class="active" onclick="toggleServiceProductDisplay(this)" data-target="sp-service">Service</span>
            <span onclick="toggleServiceProductDisplay(this)" data-target="sp-product">Product</span>
        </h2>
        <div class="line-headers">
            <span class="sp-service active"></span>
            <span class="sp-product"></span>
        </div>
    </div>
    <div class="content-sp services-wrapper sp-service active">
        <?php foreach ($services as $service) { ?>
            <div  class="service poppins" style="background: <?= $service['hex-color'] ?>; min-height: 100%; position: relative; display: flex; flex-direction: column; justify-content: space-between;">
                <div>
                    <h3><?= $service['title'] ?></h3>
                    <p><?= $service['description'] ?></p>
                </div>
                <a href="">Show More <img loading="lazy" src="./assets/icons/arrow-right.svg" alt="arrow-right"></a>
            </div>
        <?php } ?>
    </div>
    <div class="content-sp products-wrapper sp-product">
        <?php for ($i = 0; $i < 6; $i++) { ?>
            <div class="product poppins">
                <div class="image">
                    <img loading="lazy" src="/assets/images/products/platform.webp" alt="">
                </div>
                <div class="text">
                    <h3>Company Profile</h3>
                    <p>Pembuatan desain Logo, Pamflet, Banner, Poster, Brochure, CV, Catalog, Maskot, dll.</p>
                    <a href="">Show More <img loading="lazy" src="./assets/icons/arrow-right.svg" alt="arrow-right"></a>
                </div>
            </div>
        <?php } ?>
    </div>
</section>