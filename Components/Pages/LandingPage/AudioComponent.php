<div class="audio">
    <div class="button-wrap button-wrap-back-to-top d-none" id="button-back-to-top">
        <button id="button-click-audio" onclick="backToTop()">
            <i class="zmdi zmdi-chevron-up"></i>
        </button>
    </div>
    <div class="button-wrap button-wrap-audio" id="open-dialog-modal">
        <button id="button-click-audio" onclick="openModal()" everClicked="false">
            <img src="/assets/animations/cs.gif" alt="cs" loading="lazy">
        </button>
    </div>
    <div class="button-wrap button-wrap-audio d-none" onclick="closeModalDialog()" id="close-dialog-modal">
        <button id="button-click-audio" everClicked="false">
            <i class="zmdi zmdi-close"></i>
        </button>
    </div>
    <audio src="/assets/audio/bg-audio.mpeg" style="display: none;" preload="auto" loop id="bg-audio"></audio>
</div>