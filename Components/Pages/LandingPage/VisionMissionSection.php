<section id="vission-mission" class="raleway">
    <div>
        <div class="contain-wrapper">
            <h3>Visi</h3>
            <div>
                <p>Lion Of Informatics is a startup that aims to develop a technology ecosystem in Indonesia. Develop a informatics community and education to realize Indonesia as a digital country in 2035.</p>
            </div>
        </div>
        <div class="contain-wrapper">
            <h3>Misi</h3>
            <div>
                <p>Provide optimal solutions to partners in overcoming various problems in the world of information technology.</p>
                <p>Prioritizing professionalism, individual and team capabilities in producing superior products.</p>
                <p>Providing the best contribution through information and communication technology as well as community service</p>
            </div>
        </div>
    </div>
    <button>Learn More</button>
</section>