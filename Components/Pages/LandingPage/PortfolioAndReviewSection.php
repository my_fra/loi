<section id="portfolios-and-reviews-container" class="container-left">
    <div class="portfolios-categories-wrapper">
        <div class="portfolio-header">
            <h2 class="raleway">Portfolio</h2>
            <ul class="portfolio-categories portfolio-categories-flickity-1 volkhov">
                <li class="active">Platform</li>
                <li class="disabled">Design (Coming Soon)</li>
            </ul>
            <ul class="portfolio-categories portfolio-categories-flickity-2 poppins">
                <li class="active">Web</li>
                <li class="disabled">App (Coming Soon)</li>
                <li class="disabled">IoT (Coming Soon)</li>
            </ul>
        </div>
        <div class="white-space"></div>
    </div>

    <section id="portfolios-and-reviews">
        <?php require_once './Components/Pages/LandingPage/PortfolioSection.php' ?>
        <?php require_once './Components/Pages/LandingPage/ReviewSection.php' ?>
    </section>
    <button class="show-more-portfolios">Show More</button>
</section>