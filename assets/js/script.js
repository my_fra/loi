/**
 * Toggle Show Sub Features (Pricing)
 * 
 * @param element
 * 
 */
function toggleShowSubFeatures(element) {
    const parentFeaturesElement = element.parentElement;
    const activeFeatureElement = parentFeaturesElement.querySelector('li.feature.active');
    const openedSubFeaturesElement = parentFeaturesElement.querySelector('.sub_features.active');
    const targetOpenedSubFeaturesElement = parentFeaturesElement.querySelector(`.sub_features[id="${element.dataset.target}"]`);

    if (!element.classList.contains('active')) {
        if (activeFeatureElement != null) {
            activeFeatureElement.querySelector('span i').classList.remove('zmdi-chevron-up');
            activeFeatureElement.querySelector('span i').classList.add('zmdi-chevron-down');
            activeFeatureElement.classList.remove('active');
        }

        if (openedSubFeaturesElement != null) {
            openedSubFeaturesElement.classList.remove('active');
            openedSubFeaturesElement.classList.add('d-none');
        }

        element.querySelector('span i').classList.remove('zmdi-chevron-up');
        element.querySelector('span i').classList.add('zmdi-chevron-down');
        targetOpenedSubFeaturesElement.classList.remove('d-none');
        targetOpenedSubFeaturesElement.classList.add('active');
        element.classList.add('active');
    } else {
        element.querySelector('span i').classList.remove('zmdi-chevron-down');
        element.querySelector('span i').classList.add('zmdi-chevron-up');

        targetOpenedSubFeaturesElement.classList.remove('active');
        targetOpenedSubFeaturesElement.classList.add('d-none');
        element.classList.remove('active');
    }
}

function loopingHero(interval) {
    /**
     * Do looping bullet
     * 
     */
    const bullets = document.querySelectorAll('#hero .bullets .bullet-pointer');
    const text_hero_elements = document.querySelectorAll('.text-hero h2');

    setInterval(() => {
        removingPrevText();

        let onInitializationBulletIsActive = false;
        bullets.forEach((bulletElement) => {
            if (bulletElement.classList.contains('active')) {
                onInitializationBulletIsActive = true;
            }
        });

        if (!onInitializationBulletIsActive) {
            bullets[0].classList.add('active');
        }

        let mustStopped = false;

        bullets.forEach((bulletElement, i) => {
            if (!mustStopped) {
                if (bullets[0].classList.contains('active')) {
                    bullets[0].classList.remove('active');
                    bullets[1].classList.add('active');
                    text_hero_elements[0 + 1].classList.remove('d-none');

                    mustStopped = true;
                } else if (bullets[bullets.length - 1].classList.contains('active')) {
                    bullets[bullets.length - 1].classList.remove('active');
                    bullets[0].classList.add('active');

                    text_hero_elements[0].classList.remove('d-none');

                    mustStopped = true;
                } else if (bullets[i].classList.contains('active')) {
                    bullets[i].classList.remove('active');
                    bullets[i + 1].classList.add('active');

                    text_hero_elements[i + 1].classList.remove('d-none');

                    mustStopped = true;
                }
            }
        });

    }, interval);
}

function removingPrevText() {
    const text_hero_elements = document.querySelectorAll('.text-hero h2');

    text_hero_elements.forEach((e) => {
        e.classList.add('d-none');
    });
}

function toggleHamburger(hamburgerElement) {
    const navLinksNavbarElement = document.querySelector('nav ul.nav-links');

    hamburgerElement.classList.contains('show-sidebar') ? hamburgerElement.classList.remove('show-sidebar') : hamburgerElement.classList.add('show-sidebar');
    navLinksNavbarElement.classList.contains('active') ? navLinksNavbarElement.classList.remove('active') : navLinksNavbarElement.classList.add('active');
}

function toggleServiceProductDisplay(element) {
    const target = element.dataset.target;

    const lineHeaderElements = document.querySelectorAll('#service-and-product .line-headers span');
    const contentSPElements = document.querySelectorAll('#service-and-product .content-sp');

    for (let i = 0; i < lineHeaderElements.length; i++) {
        let lineHeaderElement = lineHeaderElements[i];
        let contentSPElement = contentSPElements[i];

        if (lineHeaderElement.classList.contains(target)) {
            !lineHeaderElement.classList.contains('active') ? lineHeaderElement.classList.add('active') : '';
            !contentSPElement.classList.contains('active') ? contentSPElement.classList.add('active') : '';
        } else {
            lineHeaderElement.classList.contains('active') ? lineHeaderElement.classList.remove('active') : '';
            contentSPElement.classList.contains('active') ? contentSPElement.classList.remove('active') : '';
        }
    }

    window.servicesSliders.resize();
    window.productsSliders.resize();
}

function togglePlayAudio(buttonElement, loadedAutoMaticcaly = false) {
    const bgAudioElement = document.getElementById('bg-audio');

    if (loadedAutoMaticcaly && buttonElement.getAttribute('everClicked') == 'false') {
        bgAudioElement.play();
        buttonElement.innerHTML = '<i class="zmdi zmdi-pause"></i>';
        buttonElement.setAttribute('everClicked', 'true');
    } else if (loadedAutoMaticcaly == false && buttonElement.getAttribute('everClicked') == 'true') {
        if (bgAudioElement.paused) {
            bgAudioElement.play();
            buttonElement.innerHTML = '<i class="zmdi zmdi-pause"></i>';
        } else {
            bgAudioElement.pause();
            buttonElement.innerHTML = '<i class="zmdi zmdi-play"></i>';
        }
    } else if (loadedAutoMaticcaly == false && buttonElement.getAttribute('everClicked') == 'false') {
        bgAudioElement.play();
        buttonElement.innerHTML = '<i class="zmdi zmdi-pause"></i>';
        buttonElement.setAttribute('everClicked', 'true');
    }
}

function backToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function openModal() {
    const modalDialogElement = document.getElementById('modal-dialog-cs');
    const closeModalDialogElement = document.getElementById('close-dialog-modal');
    const openModalDialogElement = document.getElementById('open-dialog-modal');

    if (!openModalDialogElement.classList.contains('d-none')) {
        openModalDialogElement.classList.add('d-none');
    }

    if (modalDialogElement.hasAttribute('close')) {
        modalDialogElement.removeAttribute('close');
    }

    if (modalDialogElement.classList.contains('d-none')) {
        modalDialogElement.classList.remove('d-none');
        modalDialogElement.setAttribute('open', true);
    } else {
        modalDialogElement.setAttribute('open', true);
    }

    if (closeModalDialogElement.hasAttribute('close')) {
        closeModalDialogElement.removeAttribute('close');
    }

    if (closeModalDialogElement.classList.contains('d-none')) {
        closeModalDialogElement.classList.remove('d-none');
        closeModalDialogElement.setAttribute('open', true);
    } else {
        closeModalDialogElement.setAttribute('open', true);
    }
}

function closeModalDialog() {
    const modalDialogElement = document.getElementById('modal-dialog-cs');
    const closeModalDialogElement = document.getElementById('close-dialog-modal');
    const openModalDialogElement = document.getElementById('open-dialog-modal');

    if (openModalDialogElement.classList.contains('d-none')) {
        openModalDialogElement.classList.remove('d-none');
    }

    if (modalDialogElement.hasAttribute('open')) {
        modalDialogElement.removeAttribute('open');
    }

    if (!modalDialogElement.classList.contains('d-none')) {
        modalDialogElement.setAttribute('close', true);
    }

    if (closeModalDialogElement.hasAttribute('open')) {
        closeModalDialogElement.removeAttribute('open');
    }

    if (!closeModalDialogElement.classList.contains('d-none')) {
        closeModalDialogElement.setAttribute('close', true);
    }
}

function responsiveBackToTopButton() {
    const buttonBackToTopElement = document.getElementById('button-back-to-top');

    if (document.documentElement.scrollTop > 0 && buttonBackToTopElement.classList.contains('d-none')) {
        buttonBackToTopElement.classList.remove('d-none');
    } else if (document.documentElement.scrollTop == 0 && !buttonBackToTopElement.classList.contains('d-none')) {
        buttonBackToTopElement.classList.add('d-none');
    }
}