<?php require_once './Components/Layouts/TopHTML.php' ?>

<!-- CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!-- End of CSS Page -->

<?php require_once './Components/Layouts/Navbar.php' ?>

<?php require_once './Components/Pages/LandingPage/HeroSection.php' ?>
<?php require_once './Components/Pages/LandingPage/VissionMissionAboutPurposeSection.php' ?>
<?php require_once './Components/Pages/LandingPage/PortfolioAndReviewSection.php' ?>
<?php require_once './Components/Pages/LandingPage/ServiceProductSection.php' ?>
<?php require_once './Components/Pages/LandingPage/PricingsSection.php' ?>
<?php require_once './Components/Pages/LandingPage/ModalDialogCs.php' ?>
<?php require_once './Components/Pages/LandingPage/AudioComponent.php' ?>

<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/tsparticles/tsparticles.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-particles"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="./assets/js/script.js?ver=1.4"></script>
<script>
    const portfolio_categories_elements = document.querySelectorAll('.portfolio-categories');
    portfolio_categories_elements.forEach((portfolio_categories_element, index) => {
        new Flickity(`.portfolio-categories-flickity-${index + 1}`, {
            draggable: true,
            freeScroll: false,
            prevNextButtons: false,
            pageDots: false,
            contain: true,
            autoPlay: false,
            pauseAutoPlayOnHover: false,
            cellAlign: 'left'
        });
    });

    const mediaCoveragesSlider = new Flickity('.media-coverages-wrapper', {
        draggable: true,
        freeScroll: false,
        prevNextButtons: false,
        pageDots: false,
        contain: true,
        autoPlay: 1500,
        pauseAutoPlayOnHover: false,
        wrapAround: true
    });

    const heroSlider = new Flickity('.hero-slider', {
        draggable: true,
        freeScroll: false,
        prevNextButtons: false,
        pageDots: false,
        contain: true,
        autoPlay: 5000,
        pauseAutoPlayOnHover: false,
        wrapAround: true
    });

    const reviewsSlider = new Flickity('.reviews', {
        draggable: true,
        freeScroll: false,
        prevNextButtons: false,
        contain: true,
        autoPlay: 2500,
        pauseAutoPlayOnHover: false,
        wrapAround: true
    });

    const portfoliosSliders = new Flickity('#portfolios-section', {
        watchCSS: true,
        prevNextButtons: false,
        wrapAround: true
    });

    window.servicesSliders = new Flickity('.services-wrapper', {
        watchCSS: true,
        prevNextButtons: false,
        wrapAround: true
    });

    window.productsSliders = new Flickity('.products-wrapper', {
        watchCSS: true,
        prevNextButtons: false,
        wrapAround: true
    });

    $(document).ready(function() {
        $('#pricings').slick({
            mobileFirst: true,
            responsive: [
                {
                breakpoint: 1000,
                settings: "unslick"
                }
            ]
        });
    });

    const particleTeams = $("#particles-js")
        .particles()
        .ajax("/assets/data/particles.json", function(container) {
            document.querySelector('.teams canvas').style.position = 'relative';
            document.querySelector('.teams canvas').style.height = 'unset';
            document.querySelector('.teams canvas').classList.add('teams-height');
        });

    loopingHero(5000);

    document.addEventListener('click', () => {
        togglePlayAudio(document.getElementById('button-toggle-music'), true);
    });
</script>
<script>
    document.addEventListener('scroll', () => {
        responsiveBackToTopButton()
    });
</script>

<?php require_once './Components/Layouts/Footer.php' ?>
<?php require_once './Components/Layouts/BottomHTML.php' ?>